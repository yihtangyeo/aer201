function readconfig(hObj, event, serialport) %#ok<INUSD>
    
    export_config = fopen('config.txt', 'w');
    
    for i=0:128

        fprintf(serialport, '%c', i);
        response = fread(serialport,1);
        fprintf(export_config, '%x', response);
        fprintf(export_config, '%c', ' ');
    
    end
    
    fclose(export_config);
end
