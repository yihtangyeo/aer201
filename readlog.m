function readlog(hObj, event, serialport) %#ok<INUSD>
    
    export_log = fopen('export_log.txt', 'w');
    
%     fprintf(serialport, '%c', 'y');
%     EEPROM_CurrentLocationH = fread(serialport,1);
%     bitH = uint8(EEPROM_CurrentLocationH);
%     fprintf(serialport, '%c', 'z');
%     EEPROM_CurrentLocationL = fread(serialport,1);
%     bitL = uint8(EEPROM_CurrentLocationL);
    
    %bytepack = uint16(bitH);
    %bytepack = bitshift(bytepack, 8);
    %bytepack = bitor(bytepack, uint6(bitL));
    
    fprintf(serialport, '%c', 'x'); % command to send data
    % start from 0x80 = 128 to bytepack
    for i=1:100000
        response = fread(serialport,1);
        if (response == 'z')
            break;
        else
            fprintf(export_log, '%c', response);    
        end
    end
    
    fclose(export_log);
end
