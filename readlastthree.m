function readlastthree(hObj, event, serialport) %#ok<INUSD>
    
    export_runs = fopen('last3run.txt', 'w');
    
    
    % -----------------------------------------------------------------
    
    fprintf(export_runs, '%s', 'Last Run #1');
    fprintf(export_runs, '\r\n');
    fprintf(export_runs, '%s', 'Date (YY/MM/DD): ');
    
    fprintf(serialport, '%c', 65);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '%c', '/');
    
    fprintf(serialport, '%c', 66);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '%c', '/');
    
    fprintf(serialport, '%c', 67);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Time (HH:MM:SS): ');
    
    fprintf(serialport, '%c', 68);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '%c', ':');
    
    fprintf(serialport, '%c', 69);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '%c', ':');
    
    fprintf(serialport, '%c', 70);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Container #1 Pattern: ');    
    fprintf(serialport, '%c', 71);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Container #2 Pattern: ');    
    fprintf(serialport, '%c', 72);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Amber Chips Remaining: ');    
    fprintf(serialport, '%c', 73);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Camel Chips Remaining: ');    
    fprintf(serialport, '%c', 74);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Time taken: ');    
    fprintf(serialport, '%c', 75);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(serialport, '%c', 76);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '%s', ' minutes ');
    
    fprintf(serialport, '%c', 77);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);    
    fprintf(serialport, '%c', 78);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '%s', ' seconds');
    fprintf(export_runs, '\r\n');
    fprintf(export_runs, '\r\n');
    
    % -------------------------------------------------
    
    fprintf(export_runs, '%s', 'Last Run #2');
    fprintf(export_runs, '\r\n');
    fprintf(export_runs, '%s', 'Date (YY/MM/DD): ');
    
    fprintf(serialport, '%c', 81);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '%c', '/');
    
    fprintf(serialport, '%c', 82);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '%c', '/');
    
    fprintf(serialport, '%c', 83);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Time (HH:MM:SS): ');
    
    fprintf(serialport, '%c', 84);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '%c', ':');
    
    fprintf(serialport, '%c', 85);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '%c', ':');
    
    fprintf(serialport, '%c', 86);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Container #1 Pattern: ');    
    fprintf(serialport, '%c', 87);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Container #2 Pattern: ');    
    fprintf(serialport, '%c', 88);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Amber Chips Remaining: ');    
    fprintf(serialport, '%c', 89);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Camel Chips Remaining: ');    
    fprintf(serialport, '%c', 90);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Time taken: ');    
    fprintf(serialport, '%c', 91);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(serialport, '%c', 92);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '%s', ' minutes ');
    
    fprintf(serialport, '%c', 93);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);    
    fprintf(serialport, '%c', 94);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '%s', ' seconds');
    fprintf(export_runs, '\r\n');
    fprintf(export_runs, '\r\n');
    
    
    % -----------------------------------------------------------
    
    fprintf(export_runs, '%s', 'Last Run #3');
    fprintf(export_runs, '\r\n');
    fprintf(export_runs, '%s', 'Date (YY/MM/DD): ');
    
    fprintf(serialport, '%c', 97);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '%c', '/');
    
    fprintf(serialport, '%c', 98);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '%c', '/');
    
    fprintf(serialport, '%c', 99);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Time (HH:MM:SS): ');
    
    fprintf(serialport, '%c', 100);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '%c', ':');
    
    fprintf(serialport, '%c', 101);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '%c', ':');
    
    fprintf(serialport, '%c', 102);
    response = fread(serialport,1);
    fprintf(export_runs, '%x', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Container #1 Pattern: ');    
    fprintf(serialport, '%c', 103);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Container #2 Pattern: ');    
    fprintf(serialport, '%c', 104);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Amber Chips Remaining: ');    
    fprintf(serialport, '%c', 105);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Camel Chips Remaining: ');    
    fprintf(serialport, '%c', 106);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '\r\n');
    
    fprintf(export_runs, '%s', 'Time taken: ');    
    fprintf(serialport, '%c', 107);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(serialport, '%c', 108);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '%s', ' minutes ');
    
    fprintf(serialport, '%c', 109);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(serialport, '%c', 110);
    response = fread(serialport,1);
    fprintf(export_runs, '%d', response);
    fprintf(export_runs, '%s', ' seconds');
    fprintf(export_runs, '\r\n');
    fprintf(export_runs, '\r\n');
    
    
    fclose(export_runs);
end